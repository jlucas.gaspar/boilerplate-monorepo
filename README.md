# How to create a new service:
0. Obs: The new service that you'll create is called `${{newServiceName}}` in this short tutorial;
1. In `packages/service-envs`, add the new service `${{newServiceName}}` in `enum ServicesNames` and then in the `const Service`;
2. In `packages/services-types`, copy and paste the base.ts with the new `${{newServiceName}}`. (Tip -> run in terminal `cp -r Base/ ${{newServiceName}}/`);
3. In `services` folder, copy and paste `Base/` folder with the new service name. (Tip -> run in terminal `cp -r base/ ${{newServiceName}}/`);
4. In your new `${{newServiceName}}/utils/serviceInfo.ts` change the variable Service.base to `const service = ServicesNames.${{newServiceName}}`;
5. Change your new package.json "name" to `monorepo-boilerplate-${{newServiceName}}`;
6. run `lerna bootstrap` in your new service folder and run `npm start`;