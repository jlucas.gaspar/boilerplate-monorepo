import { User } from '@monorepo-boilerplate/types/user';
import { Service, ServicesNames } from '@monorepo-boilerplate/env';
import { createAxiosInstance } from './utils/axiosInstance';

type Param = {
  internalApiKey: string;
  mode: string;
}

type Model = User.Model.Complete;
type GetUserById = User.HttpRequest.GetById;
type GetByEmail = User.HttpRequest.GetByEmail;
type Create = User.HttpRequest.Create;
type Update = User.HttpRequest.Update;
type DeleteById = User.HttpRequest.DeleteById;

type Response = {
  ping: () => Promise<any>;
  getUserById: (input: GetUserById) => Promise<Model | undefined>;
  getUserByEmail: (input: GetByEmail) => Promise<Model | undefined>;
  createUser: (input: Create) => Promise<Model>;
  updateUser: (input: Update) => Promise<Model>;
  deleteUserById: (input: DeleteById) => Promise<Model>;
}

export const createUserService = ({ internalApiKey, mode }: Param): Response => {
  const serviceName = ServicesNames.User;
  const { port } = Service[serviceName];
  const baseUrl = mode === 'dev' ? `http://localhost:${port}/${serviceName}` : ''; //! ajeitar esse '';

  const { get, post, put, delete: softDelete } = createAxiosInstance({ internalApiKey, baseUrl });

  return {
    ping: async () => await get('/public/ping'),

    getUserById: async ({ id }: GetUserById): Promise<Model | undefined> => {
      const result = await get(`/internal/by-id/${id}`);
      return result.data;
    },

    getUserByEmail: async ({ email }: GetByEmail): Promise<Model | undefined> => {
      const result = await get(`/internal/by-email/${email}`);
      return result.data;
    },

    createUser: async (userData: Create): Promise<Model> => {
      const result = await post('/internal', userData);
      return result.data;
    },

    updateUser: async ({ id, ...userData }: Update): Promise<Model> => {
      const result = await put(`/internal/${id}`, userData);
      return result.data;
    },

    deleteUserById: async ({ id }: DeleteById): Promise<Model> => {
      const result = await softDelete(`/internal/${id}`);
      return result.data;
    }
  }
}