import axios from 'axios'

type Param = {
  internalApiKey: string;
  baseUrl: string;
}

export const createAxiosInstance = ({ internalApiKey, baseUrl }: Param) => axios.create({
  baseURL: baseUrl,
  headers: {
    'Internal-Api-Key': internalApiKey
  }
});