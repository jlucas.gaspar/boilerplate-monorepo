import { Auth } from '@monorepo-boilerplate/types/auth';
import { Service, ServicesNames } from '@monorepo-boilerplate/env';
import { createAxiosInstance } from './utils/axiosInstance';

type Param = {
  internalApiKey: string;
  mode: string
}

type JwtToken = Auth.Helpers.JwtToken;
type LoginWithEmail = Auth.HttpRequest.LoginWithEmail;
type SignUpWithEmail = Auth.HttpRequest.SignUpWithEmail;
type AuthenticateWithGoogle = Auth.HttpRequest.AuthenticateWithGoogle;
type RefreshToken = Auth.HttpRequest.RefreshToken;
type SendForgotPasswordEmail = Auth.HttpRequest.SendForgotPasswordEmail;
type ResetPassword = Auth.HttpRequest.ResetPassword;

type Response = {
  ping: () => any;
  loginWithEmail: (input: LoginWithEmail) => Promise<JwtToken>;
  signUpWithEmail: (input: SignUpWithEmail) => Promise<JwtToken>;
  authenticateWithGoogle: (input: AuthenticateWithGoogle) => Promise<JwtToken>;
  refreshToken: (input: RefreshToken) => Promise<JwtToken>;
  sendForgotPasswordEmail: (input: SendForgotPasswordEmail) => Promise<string>;
  resetPassword: (input: ResetPassword) => Promise<JwtToken>;
}

export const createAuthService = ({ internalApiKey, mode }: Param): Response => {
  const serviceName = ServicesNames.Auth;
  const { port } = Service[serviceName];
  const baseUrl = mode === 'dev' ? `http://localhost:${port}/${serviceName}` : ''; //! ajeitar esse '';

  const { get, post } = createAxiosInstance({ internalApiKey, baseUrl });

  return {
    ping: async () => await get('/public/ping'),

    loginWithEmail: async (input: LoginWithEmail): Promise<JwtToken> => {
      const response = await post('/internal/login-email', input);
      return response.data;
    },

    signUpWithEmail: async (input: SignUpWithEmail): Promise<JwtToken> => {
      const response = await post('/internal/signup-email', input);
      return response.data;
    },

    authenticateWithGoogle: async (input: AuthenticateWithGoogle): Promise<JwtToken> => {
      const response = await post('/internal/authenticate-google', input);
      return response.data;
    },

    refreshToken: async ({ userId }: RefreshToken): Promise<JwtToken> => {
      const response = await post('/internal/refresh-token', { userId });
      return response.data;
    },

    sendForgotPasswordEmail: async ({ email }: SendForgotPasswordEmail): Promise<string> => {
      const response = await post('/internal/forgot-password', { email });
      return response.data;
    },

    resetPassword: async (resetPasswordData: ResetPassword): Promise<JwtToken> => {
      const response = await post('/internal/reset-password', resetPasswordData);
      return response.data;
    }
  }
}