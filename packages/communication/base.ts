import { Service, ServicesNames } from '@monorepo-boilerplate/env';
import { createAxiosInstance } from './utils/axiosInstance';

type Param = {
  internalApiKey: string;
  mode: string;
}

type Response = {
  ping: () => Promise<any>;
}

export const createBaseService = ({ internalApiKey, mode }: Param): Response => {
  const serviceName = ServicesNames.Auth;
  const { port } = Service[serviceName];
  const baseUrl = mode === 'dev' ? `http://localhost:${port}/${serviceName}` : ''; //! ajeitar esse '';

  const { get } = createAxiosInstance({ internalApiKey, baseUrl });

  return {
    ping: async () => await get('/public/ping')
  }
}