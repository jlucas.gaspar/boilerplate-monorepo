import knex, { Knex } from 'knex';

export type DatabaseConfig = {
    development: Knex.Config;
    production: Knex.Config;
}

export const initDatabase = (env: any, knexfile: DatabaseConfig): Knex<any, unknown[]> => {
    if (env.mode === 'dev') return knex(knexfile.development);
    else return knex(knexfile.production);
}

export { Knex }