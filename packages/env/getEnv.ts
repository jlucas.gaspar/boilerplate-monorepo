import { MissingError } from '@monorepo-boilerplate/error';

export const getEnv = (name: string, envObj: any): string => {
  const devOrProd = envObj.MODE;

  const modeIsCorrect = devOrProd === 'dev' || devOrProd === 'prod';
  if (!modeIsCorrect) throw new MissingError('process.env.MODE must be "dev" or "prod".');

  const name_mode = `${name}_${devOrProd}`;

  if (name === 'MODE') return devOrProd;
  else return envObj[name_mode] || '';
}