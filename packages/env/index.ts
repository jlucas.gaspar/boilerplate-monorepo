import dotenv from 'dotenv';

export * from './serviceEnvs';
export * from './getEnv';

export const initializeDotenv = () => dotenv.config();