import { RequestHandler } from 'express';

export const routeNotFound: RequestHandler = (request, response) => {
  return response.status(404).json({
    errors: [`Route ${request.originalUrl} not found.`]
  });
}