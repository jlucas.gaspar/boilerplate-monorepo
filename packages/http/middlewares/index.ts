import { User } from '@monorepo-boilerplate/types/user';

declare global {
  namespace Express {
    interface Request {
      user: User.Model.Complete;
    }
  }
}
export * from './verifyInternalApiKey';
export * from './jwtDecoder';
export * from './errorHandler';
export * from './getRequestUser';
export * from './routeNotFound';