import dotenv from 'dotenv';
import { getEnv } from '@monorepo-boilerplate/env';
import { createUserService } from '@monorepo-boilerplate/communication/user';

dotenv.config();

const mode = getEnv('MODE', process.env);
const internalApiKey = getEnv('INTERNAL_API_KEY', process.env);

export const userService = createUserService({ internalApiKey, mode });