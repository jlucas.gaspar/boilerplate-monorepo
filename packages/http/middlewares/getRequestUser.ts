import { Request, Response, NextFunction } from 'express';
import { BadRequestError } from '@monorepo-boilerplate/error';
import { userService } from './utils/userServiceCommunication';

export const getRequestUser = async (request: Request, response: Response, next: NextFunction) => {
  const isPublic = request.originalUrl.includes('/public/');
  const isInternal = request.originalUrl.includes('/internal/');

  if (isPublic || isInternal) {
    return next();
  }

  const { id } = request.user!;

  const user = await userService.getUserById({ id });

  if (!user) {
    throw new BadRequestError('Erro ao buscar o usuário referente a essa requisição.');
  }

  request.user = user;

  return next();
}