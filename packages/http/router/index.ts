// import multer from 'multer';
import { Router, RequestHandler } from 'express';
import { celebrate } from 'celebrate';
import { ValidationOptions } from 'joi';

interface RouteHandlerParams {
  handler: RequestHandler;
  // router: Router;
  schema?: object;
  schemaOptions?: ValidationOptions;
  updloadFilename?: string;
}

export const createRouter = (): Router => Router();

export const routerHandler = (router: Router) => {
  // const uploadMiddleware = multer();

  return {
    post: (path: string, { schema, schemaOptions, updloadFilename, handler }: RouteHandlerParams) => {
      // const upload = updloadFilename ? uploadMiddleware.single(updloadFilename) : null;
      const upload = null;

      if (schema && upload) return router.post(path, celebrate({ body: schema }, { ...schemaOptions, abortEarly: false }), upload, handler);
      else if (schema && !upload) return router.post(path, celebrate({ body: schema }, { ...schemaOptions, abortEarly: false }), handler);
      else if (!schema && upload) return router.post(path, upload, handler);
      else return router.post(path, handler);
    },

    get: (path: string, { schema, schemaOptions, updloadFilename, handler }: RouteHandlerParams) => {
      // const upload = updloadFilename ? uploadMiddleware.single(updloadFilename) : null;
      const upload = null;

      if (schema && upload) return router.get(path, celebrate({ body: schema }, { ...schemaOptions, abortEarly: false }), upload, handler);
      else if (schema && !upload) return router.get(path, celebrate({ body: schema }, { ...schemaOptions, abortEarly: false }), handler);
      else if (!schema && upload) return router.get(path, upload, handler);
      else return router.get(path, handler);
    },

    put: (path: string, { schema, schemaOptions, updloadFilename, handler }: RouteHandlerParams) => {
      // const upload = updloadFilename ? uploadMiddleware.single(updloadFilename) : null;
      const upload = null;

      if (schema && upload) return router.put(path, celebrate({ body: schema }, { ...schemaOptions, abortEarly: false }), upload, handler);
      else if (schema && !upload) return router.put(path, celebrate({ body: schema }, { ...schemaOptions, abortEarly: false }), handler);
      else if (!schema && upload) return router.put(path, upload, handler);
      else return router.put(path, handler);
    },

    delete: (path: string, { schema, schemaOptions, updloadFilename, handler }: RouteHandlerParams) => {
      // const upload = updloadFilename ? uploadMiddleware.single(updloadFilename) : null;
      const upload = null;

      if (schema && upload) return router.delete(path, celebrate({ body: schema }, { ...schemaOptions, abortEarly: false }), upload, handler);
      else if (schema && !upload) return router.delete(path, celebrate({ body: schema }, { ...schemaOptions, abortEarly: false }), handler);
      else if (!schema && upload) return router.delete(path, upload, handler);
      else return router.delete(path, handler);
    }
  }
}
// import { Router } from 'express';
// import { celebrate } from 'celebrate';
// import { Http } from '@monorepo-boilerplate/types/shared';

// type RouterOptions = {
//   [name: string]: {
//     method: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH';
//     path: string;
//     handler: (request: Http.Request<any>, response: Http.Response) => Promise<Http.Response>;
//     schema?: object;
//   }
// }

// export const createRouter = (options: RouterOptions) => {
//   const router = Router();

//   for (const [name, data] of Object.entries(options)) {
//     const { path, method, handler, schema } = data;

//     type MethodLowerCase = 'get' | 'post' | 'put' | 'delete' | 'patch';
//     const methodLowerCase = method.toLowerCase() as MethodLowerCase;

//     router[methodLowerCase](path, celebrate({ body: schema }), handler);
//   }

//   return router;
// }