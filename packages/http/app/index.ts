import express, { Express } from 'express';

export const initializeApp = (): Express => express();

export const bodyParserJson = () => express.json();