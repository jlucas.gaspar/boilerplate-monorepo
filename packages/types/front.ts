// import { Dates, Utils } from './shared';

export namespace Front {
  export namespace Model {}

  export namespace HttpRequest {}

  export namespace Core {}

  export namespace Database {}

  export namespace Helpers {}
}