// import { Dates, Utils } from './shared';

export namespace Base {
  export namespace Model {}

  export namespace HttpRequest {}

  export namespace Core {}

  export namespace Database {}

  export namespace Helpers {}
}