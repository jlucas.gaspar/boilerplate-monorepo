import { Dates } from './shared';

export namespace Auth {
  export namespace Model {
    export type ResetPassword = Dates & {
      resetPasswordToken: string;
      email: string;
    }
  }

  export namespace HttpRequest {
    export type LoginWithEmail = Helpers.EmailAndPassword;
    export type SignUpWithEmail = Helpers.PasswordAndPasswordConfirmation & { name: string; email: string; }
    export type AuthenticateWithGoogle = Helpers.GoogleCredentials;
    export type ResetPassword = Pick<Model.ResetPassword, 'resetPasswordToken'> & Helpers.PasswordAndPasswordConfirmation;
    export type SendForgotPasswordEmail = { email: string; }
    export type RefreshToken = { userId: string; }
  }

  export namespace Core {
    export type LoginWithEmail = HttpRequest.LoginWithEmail;
    export type SignUpWithEmail = Omit<HttpRequest.SignUpWithEmail, 'passwordConfirmation'>;
    export type AuthenticateWithGoogle = HttpRequest.AuthenticateWithGoogle;
    export type ResetPassword = Omit<HttpRequest.ResetPassword, 'passwordConfirmation'>;
    export type SendForgotPasswordEmail = HttpRequest.SendForgotPasswordEmail;
    export type RefreshToken = HttpRequest.RefreshToken;
  }

  export namespace Database {}

  export namespace Helpers {
    export type GoogleCredentials = { email: string; providerId: string; name: string; photoUrl?: string; phoneNumber?: string }
    export type EmailAndPassword = { email: string; password: string; }
    export type PasswordAndPasswordConfirmation = { password: string; passwordConfirmation: string; }
    export type JwtToken = { exp: number; token: string; }
    export type DecodedJwtToken = { exp: number; sub: string; }
  }
}