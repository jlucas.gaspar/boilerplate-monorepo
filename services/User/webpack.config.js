const path = require('path');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const { NormalModuleReplacementPlugin } = require('webpack');

module.exports = {
  mode: 'production',
  entry: './src/index.ts',
  target: 'node',
  output: {
    libraryTarget: 'commonjs',
    filename: 'index.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [{
      test: /\.ts$/,
      use: { loader: 'ts-loader', options: { transpileOnly: true, configFile: 'tsconfig.json' } }
    }]
  },
  resolve: {
    extensions: ['.ts', '.js'],
    plugins: [new TsconfigPathsPlugin({ configFile: './tsconfig.json' })]
  },
  optimization: {
    nodeEnv: false
  },
  plugins: [ // Ignore knex runtime drivers that we don't use
    new NormalModuleReplacementPlugin(/m[sy]sql2?|oracle(db)?|sqlite3|pg-(native|query)/, 'noop2')
  ]
}