import { Knex } from '@monorepo-boilerplate/database/knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('users', function (table: Knex.TableBuilder) {
    // Primary Key;
    table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'));

    // Not Nullable fields;
    table.string('name').notNullable();
    table.string('email').notNullable();
    table.string('provider').notNullable();

    // Nullable fields;
    table.string('password').nullable();
    table.string('providerId').nullable();
    table.string('photoUrl').nullable();
    table.string('phoneNumber').nullable();
    table.uuid('companyId').nullable();

    // Timestamps;
    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').defaultTo(knex.fn.now());
    table.timestamp('deletedAt').nullable();

    // Uniques fields;
    table.unique(['email']);
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('users');
}