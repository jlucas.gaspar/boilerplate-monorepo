import { Service, ServicesNames } from '@monorepo-boilerplate/env';

const service = ServicesNames.User;

export const serviceName = service;
export const servicePort = Service[serviceName].port;