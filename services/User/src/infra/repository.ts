import { initDatabase } from '@monorepo-boilerplate/database/knex';
import { User } from '@monorepo-boilerplate/types/user';
import { env } from '../config/env';
import knexfile from '../../knexfile';

type Model = User.Model.Complete;
type InsertParams = User.Database.Insert;
type UpdateParams = User.Database.Update;

const database = initDatabase(env, knexfile);
const usersTable = () => database<Model>('users');

export const insertUser = async (params: InsertParams): Promise<Model> => {
  return await usersTable().insert(params).returning('*').then(account => account[0]);
}

export const updateUser = async (params: UpdateParams): Promise<Model | undefined> => {
  const { id, ...updateData } = params;
  const data = { ...updateData, updatedAt: database.fn.now() };
  return await usersTable().where('id', id).whereNull('deletedAt').update(data).returning('*').then(account => account[0]);
}

export const findUserById = async (id: string): Promise<Model | undefined> => {
  return await usersTable().where('id', id).whereNull('deletedAt').first();
}

export const findUserByEmail = async (email: string): Promise<Model | undefined> => {
  return await usersTable().where('email', email).whereNull('deletedAt').first();
}

export const deleteUserById = async (id: string): Promise<Model | undefined> => {
  return await updateUser({ id, deletedAt: database.fn.now() } as any);
}