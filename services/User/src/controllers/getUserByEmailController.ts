import { Http } from '@monorepo-boilerplate/http/types';
import { User } from '@monorepo-boilerplate/types/user';
import { getUserByEmail } from '../core';

type Input = Http.Request<User.HttpRequest.GetByEmail>;
type Output = Http.Response<User.Model.WithoutPassword>;

export const getUserByEmailController = async (request: Input, response: Output): Promise<Output> => {
  const { email } = request.params;
  const user = await getUserByEmail({ email });
  return response.status(200).json(user);
}