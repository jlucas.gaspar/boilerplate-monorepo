import { Http } from '@monorepo-boilerplate/http/types';
import { User } from '@monorepo-boilerplate/types/user';
import { deleteUserById } from '../core';

type Input = Http.Request<User.HttpRequest.GetById>;
type Output = Http.Response<User.Model.WithoutPassword>;

export const deleteUserByIdController = async (request: Input, response: Output): Promise<Output> => {
  const { id } = request.params;
  const user = await deleteUserById({ id });
  return response.status(200).json(user);
}