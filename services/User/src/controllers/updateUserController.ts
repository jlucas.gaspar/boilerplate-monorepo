import { Http } from '@monorepo-boilerplate/http/types';
import { User } from '@monorepo-boilerplate/types/user';
import { updateUser } from '../core';

type Input = Http.Request<User.HttpRequest.Update>;
type Output = Http.Response<User.Model.WithoutPassword>;

export const updateUserController = async (request: Input, response: Output): Promise<Output> => {
  const { id } = request.params;
  const { email, providerId, password, photoUrl, phoneNumber, name, provider } = request.body;
  const user = await updateUser({ provider, name, phoneNumber, photoUrl, password, providerId, email, id });
  return response.status(200).json(user);
}