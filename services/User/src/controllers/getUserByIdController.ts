import { Http } from '@monorepo-boilerplate/http/types';
import { User } from '@monorepo-boilerplate/types/user';
import { getUserById } from '../core';

type Input = Http.Request<User.HttpRequest.GetById>;
type Output = Http.Response<User.Model.WithoutPassword>;

export const getUserByIdController = async (request: Input, response: Output): Promise<Output> => {
  const { id } = request.params;
  const user = await getUserById({ id });
  return response.status(200).json(user);
}