import { Http } from '@monorepo-boilerplate/http/types';
import { User } from '@monorepo-boilerplate/types/user';
import { createUser } from '../core';

type Input = Http.Request<User.HttpRequest.Create>;
type Output = Http.Response<User.Model.WithoutPassword>;

export const createUserController = async (request: Input, response: Output): Promise<Output> => {
  const { email, name, phoneNumber, photoUrl, provider, password, providerId } = request.body;
  const user = await createUser({ email, name, phoneNumber, photoUrl, provider, password, providerId });
  return response.status(201).json(user);
}