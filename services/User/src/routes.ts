import { createRouter, routerHandler } from '@monorepo-boilerplate/http/router';
import { User } from '@monorepo-boilerplate/types/user';
import * as Validator from '@monorepo-boilerplate/http/validator';
import * as handlers from './controllers';

const { Joi } = Validator;
const router = createRouter();
const { post, put, delete: softDelete, get } = routerHandler(router);

// Health Check (PING) => GET :: /user/public/ping
get('/public/ping', {
  handler: handlers.pingController,
  schema: Joi.object<void>()
});

// Get User By Email => GET :: /user/internal/by-email/:email
get('/internal/by-email/:email', {
  handler: handlers.getUserByEmailController,
  schema: Joi.object<void>()
});

// Get User By Id => GET :: /user/internal/by-id/:id
get('/internal/by-id/:id', {
  handler: handlers.getUserByIdController,
  schema: Joi.object<void>()
});

// Create User => POST :: /user/internal
post('/internal', {
  handler: handlers.createUserController,
  schema: Joi.object<User.HttpRequest.Create>({
    email: Joi.string().email().required(),
    name: Joi.string().optional(),
    password: Joi.string().optional(),
    phoneNumber: Joi.string().optional(),
    photoUrl: Joi.string().optional(),
    provider: Joi.string().valid('google', 'email').required(),
    providerId: Joi.string().optional()
  })
});

// Update User => PUT :: /user/internal/:id
put('/internal/:id', {
  handler: handlers.updateUserController,
  schema: Joi.object<User.HttpRequest.Update>({
    email: Joi.string().email().optional(),
    name: Joi.string().optional(),
    password: Joi.string().optional(),
    phoneNumber: Joi.string().optional(),
    photoUrl: Joi.string().optional(),
    provider: Joi.string().valid('google', 'email').optional(),
    providerId: Joi.string().optional()
  })
});

// Delete User => DELETE :: /user/internal/:id
softDelete('/internal/:id', {
  handler: handlers.deleteUserByIdController,
  schema: Joi.object<void>()
});

export const routes = router;