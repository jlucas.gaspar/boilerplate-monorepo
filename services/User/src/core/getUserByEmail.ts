import { omit } from 'ramda';
import { User } from '@monorepo-boilerplate/types/user';
import * as usersRepository from '../infra/repository';

type Input = User.Core.GetByEmail & { returnPassword?: boolean; }
type Output = User.Model.WithoutPassword | User.Model.Complete | undefined;

export const getUserByEmail = async ({ email, returnPassword }: Input): Promise<Output> => {
  const user = await usersRepository.findUserByEmail(email);

  if (!user) return undefined;

  if (returnPassword) return user;
  else return omit(['password'], user);
}