import { omit } from 'ramda';
import { BadRequestError } from '@monorepo-boilerplate/error';
import { User } from '@monorepo-boilerplate/types/user';
import * as usersRepository from '../infra/repository';

type Input = User.Core.Create;
type Output = User.Model.WithoutPassword;

export const createUser = async (userData: Input): Promise<Output> => {
  const emailAlreadyExists = await usersRepository.findUserByEmail(userData.email);

  if (emailAlreadyExists) {
    throw new BadRequestError('E-mail já está em uso.');
  }

  const user = await usersRepository.insertUser(userData);

  return omit(['password'], user);
}