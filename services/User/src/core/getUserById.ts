import { omit } from 'ramda';
import { User } from '@monorepo-boilerplate/types/user';
import * as usersRepository from '../infra/repository';

type Input = User.Core.GetById & { returnPassword?: boolean; }
type Output = User.Model.WithoutPassword | User.Model.Complete | undefined;

export const getUserById = async ({ id, returnPassword }: Input): Promise<Output> => {
  const user = await usersRepository.findUserById(id);

  if (!user) return undefined;

  if (returnPassword) return user;
  else return omit(['password'], user);
}