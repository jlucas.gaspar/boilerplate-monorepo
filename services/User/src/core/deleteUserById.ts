import { omit } from 'ramda';
import { BadRequestError } from '@monorepo-boilerplate/error';
import { User } from '@monorepo-boilerplate/types/user';
import * as usersRepository from '../infra/repository';

type Input = User.Core.DeleteById;
type Output = User.Model.WithoutPassword;

export const deleteUserById = async ({ id }: Input): Promise<Output> => {
  const deletedUser = await usersRepository.deleteUserById(id);

  if (!deletedUser) {
    throw new BadRequestError('Usuário não enontrado.');
  }

  return omit(['password'], deletedUser);
}