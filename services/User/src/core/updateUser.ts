import { omit } from 'ramda';
import { BadRequestError } from '@monorepo-boilerplate/error';
import { User } from '@monorepo-boilerplate/types/user';
import * as usersRepository from '../infra/repository';

type Input = User.Core.Update;
type Output = User.Model.WithoutPassword;

export const updateUser = async (userData: Input): Promise<Output> => {
  if (userData.email) {
    const emailAlreadyInUse = await usersRepository.findUserByEmail(userData.email);
    if (emailAlreadyInUse?.id !== userData.id) {
      throw new BadRequestError('E-mail já está em uso.');
    }
  }

  const updatedUser = await usersRepository.updateUser(userData);

  if (!updatedUser) {
    throw new BadRequestError('Usuário não enontrado.');
  }

  return omit(['password'], updatedUser);
}