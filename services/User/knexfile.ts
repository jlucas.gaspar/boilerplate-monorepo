import { initializeDotenv } from '@monorepo-boilerplate/env';
import { DatabaseConfig } from '@monorepo-boilerplate/database/knex';
import { serviceName } from './src/utils/serviceInfo';

initializeDotenv();

const { env } = process;

const knexfile: DatabaseConfig = {
  development: {
    client: env.DB_CLIENT_dev,
    connection: {
      host: env.DB_HOST_dev,
      port: Number(env.DB_PORT_dev),
      database: env.DB_NAME_dev,
      user: env.DB_USER_dev,
      password: env.DB_PASSWORD_dev
    },
    migrations: {
      tableName: `migrations_${serviceName}_service`,
      directory: './migrations'
    }
  },

  production: {
    client: 'pg',
    useNullAsDefault: true,
    connection: {
      database: 'truckify',
      user: 'truckify',
      password: 'truckify'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: `migrations_${serviceName}_service`
    }
  }
}

export default knexfile;