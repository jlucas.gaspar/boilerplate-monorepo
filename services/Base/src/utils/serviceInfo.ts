import { Service, ServicesNames } from '@monorepo-boilerplate/env';

const service = ServicesNames.Base;

export const serviceName = service;
export const servicePort = Service[serviceName].port;