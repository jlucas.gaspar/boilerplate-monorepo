import { initializeApp, bodyParserJson } from '@monorepo-boilerplate/http/app';
import * as middleware from '@monorepo-boilerplate/http/middlewares';
import { serviceName } from './utils/serviceInfo';
import { env } from './config/env';
import { routes } from './routes';

const app = initializeApp();

app.use(bodyParserJson());
app.use(middleware.verifyInternalApiKey(env.internalApiKey));
app.use(middleware.jwtDecoder(env.jwtSecret));
app.use(middleware.getRequestUser);
app.use(`/${serviceName}`, routes);
app.use(middleware.routeNotFound);
app.use(middleware.errorHandler);

export { app }