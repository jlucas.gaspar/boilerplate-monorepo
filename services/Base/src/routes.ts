import { createRouter, routerHandler } from '@monorepo-boilerplate/http/router';
import * as Validator from '@monorepo-boilerplate/http/validator';
import * as handlers from './controllers';

const { Joi } = Validator;
const router = createRouter();
const { post, put, delete: softDelete, get } = routerHandler(router);

// Health Check (PING) => GET :: /base/public/ping
get('/public/ping', {
  handler: handlers.pingController,
  schema: Joi.object<void>()
});

export const routes = router;