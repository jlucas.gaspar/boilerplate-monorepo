import { Knex } from '@monorepo-boilerplate/database/knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('resetPasswordTokens', function (table: Knex.TableBuilder) {
    // Not Nullable fields;
    table.uuid('resetPasswordToken').defaultTo(knex.raw('uuid_generate_v4()'));
    table.string('email').notNullable();

    // Timestamps;
    table.timestamp('createdAt').defaultTo(knex.fn.now());
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('resetPasswordTokens');
}