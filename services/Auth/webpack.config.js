const path = require('path');
const nodeExternals = require('webpack-node-externals');
const { NormalModuleReplacementPlugin } = require('webpack');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  entry: './src/index.ts',
  target: 'node',
  externals: [nodeExternals()],
  output: {
    libraryTarget: 'commonjs2',
    filename: 'index.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [{
      test: /\.ts$/,
      use: { loader: 'ts-loader', options: { transpileOnly: true, configFile: 'tsconfig.json' } }
    }]
  },
  resolve: {
    extensions: ['.js', '.ts'],
    plugins: [new TsconfigPathsPlugin({ configFile: './tsconfig.json' })]
  },
  plugins: [ // Ignore knex dynamic required dialects that we don't use
    new NormalModuleReplacementPlugin(/m[sy]sql2?|oracle(db)?|sqlite3|pg-(native|query)/, 'noop2')
  ]
}