import { Http } from '@monorepo-boilerplate/http/types';
import { Auth } from '@monorepo-boilerplate/types/auth';
import { signUpWithEmail } from '../core';

type Input = Http.Request<Auth.HttpRequest.SignUpWithEmail>;
type Output = Http.Response<Auth.Helpers.JwtToken>;

export const signUpWithEmailController = async (request: Input, response: Output): Promise<Output> => {
  const { email, name, password } = request.body;
  const { exp, token } = await signUpWithEmail({ email, name, password });
  return response.status(200).json({ exp, token });
}