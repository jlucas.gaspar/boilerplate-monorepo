import { Http } from '@monorepo-boilerplate/http/types';
import { Auth } from '@monorepo-boilerplate/types/auth';
import { loginWithEmail } from '../core';

type Input = Http.Request<Auth.HttpRequest.LoginWithEmail>;
type Output = Http.Response<Auth.Helpers.JwtToken>;

export const loginWithEmailController = async (request: Input, response: Output): Promise<Output> => {
  const { email, password } = request.body;
  const { exp, token } = await loginWithEmail({ email, password });
  return response.status(200).json({ exp, token });
}