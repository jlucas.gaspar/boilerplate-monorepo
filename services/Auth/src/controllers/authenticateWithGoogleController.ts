import { Http } from '@monorepo-boilerplate/http/types';
import { Auth } from '@monorepo-boilerplate/types/auth';
import { authenticateWithGoogle } from '../core';

type Input = Http.Request<Auth.HttpRequest.AuthenticateWithGoogle>;
type Output = Http.Response<Auth.Helpers.JwtToken>;

export const authenticateWithGoogleController = async (request: Input, response: Output): Promise<Output> => {
  const { email, name, photoUrl, phoneNumber, providerId } = request.body;
  const { exp, token } = await authenticateWithGoogle({ email, name, photoUrl, phoneNumber, providerId });
  return response.status(200).json({ exp, token });
}