import { Http } from '@monorepo-boilerplate/http/types';
import { Auth } from '@monorepo-boilerplate/types/auth';
import { sendForgotPasswordEmail } from '../core';

type Input = Http.Request<Auth.HttpRequest.SendForgotPasswordEmail>;
type Output = Http.Response<string>;

export const sendForgotPasswordEmailController = async (request: Input, response: Output): Promise<Output> => {
  const { email } = request.body;
  const emailIsSent = await sendForgotPasswordEmail(email);
  return response.status(200).json(emailIsSent);
}