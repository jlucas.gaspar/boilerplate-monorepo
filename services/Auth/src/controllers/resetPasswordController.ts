import { Http } from '@monorepo-boilerplate/http/types';
import { Auth } from '@monorepo-boilerplate/types/auth';
import { resetPassword } from '../core';

type Request = Http.Request<Auth.HttpRequest.ResetPassword>;
type Response = Http.Response<Auth.Helpers.JwtToken>;

export const resetPasswordController = async (request: Request, response: Response): Promise<Response> => {
  const { password, resetPasswordToken } = request.body;
  const { exp, token } = await resetPassword({ password, resetPasswordToken });
  return response.status(200).json({ exp, token });
}