import { Http } from '@monorepo-boilerplate/http/types';
import { Auth } from '@monorepo-boilerplate/types/auth';
import { refreshToken } from '../core';

type Request = Http.Request<Auth.HttpRequest.RefreshToken>;
type Response = Http.Response<Auth.Helpers.JwtToken>;

export const refreshTokenController = async (request: Request, response: Response): Promise<Response> => {
  const { userId } = request.body;
  const { exp, token } = refreshToken({ userId });
  return response.status(200).json({ exp, token });
}