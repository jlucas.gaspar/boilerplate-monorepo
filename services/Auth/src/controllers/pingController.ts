import { Http } from '@monorepo-boilerplate/http/types';
import { ping } from '../core';

type Input = Http.Request<void>;
type Output = Http.Response<any>;

export const pingController = async (request: Input, response: Output): Promise<Output> => {
  const { body } = request;
  const pong = await ping(body);
  return response.status(200).json(pong);
}