export const ensureResetPasswordTokenIsValid = (tokenCreatedAt: Date): boolean => {
  const twoHours = 1000 * 60 * 60 * 2;
  const validationTime = new Date(tokenCreatedAt).getTime() + twoHours;

  if (Date.now() > validationTime) return false;
  else return true;
}