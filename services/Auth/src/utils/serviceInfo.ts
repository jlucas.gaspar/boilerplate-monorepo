import { Service, ServicesNames } from '@monorepo-boilerplate/env';

const service = ServicesNames.Auth;

export const serviceName = service;
export const servicePort = Service[serviceName].port;