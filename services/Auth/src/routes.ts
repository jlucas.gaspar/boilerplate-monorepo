import { createRouter, routerHandler } from '@monorepo-boilerplate/http/router';
import { Auth } from '@monorepo-boilerplate/types/auth';
import * as Validator from '@monorepo-boilerplate/http/validator';
import * as handlers from './controllers';

const { Joi } = Validator;
const router = createRouter();
const { post, get } = routerHandler(router);

// Health Check (PING) => GET :: /auth/public/ping
get('/public/ping', {
  handler: handlers.pingController,
  schema: Joi.object<void>()
});

// Login With Email => POST :: /auth/internal/login-email
post('/internal/login-email', {
  handler: handlers.loginWithEmailController,
  schema: Joi.object<Auth.HttpRequest.LoginWithEmail>({
    email: Joi.string().email().required(),
    password: Joi.string().email().required()
  })
});

// SignUp With Email => POST :: /auth/internal/signup-email
post('/internal/signup-email', {
  handler: handlers.signUpWithEmailController,
  schema: Joi.object<Auth.HttpRequest.SignUpWithEmail>({
    email: Joi.string().email().required(),
    name: Joi.string().email().required(),
    password: Joi.string().email().required(),
    passwordConfirmation: Joi.string().valid(Joi.ref('password')).required()
  })
});

// Authenticate With Google => POST :: /auth/internal/authenticate-google
post('/internal/authenticate-google', {
  handler: handlers.authenticateWithGoogleController,
  schema: Joi.object<Auth.HttpRequest.AuthenticateWithGoogle>({
    providerId: Joi.string().email().required(),
    email: Joi.string().email().required(),
    name: Joi.string().email().optional(),
    phoneNumber: Joi.string().email().optional(),
    photoUrl: Joi.string().email().optional()
  })
});

// Refresh Token => POST :: /auth/internal/refresh-token
post('/internal/refresh-token', {
  handler: handlers.refreshTokenController,
  schema: Joi.object<Auth.HttpRequest.RefreshToken>({
    userId: Joi.string().email().required()
  })
});

// Send ForgotPasswordToken E-mail => POST :: /auth/internal/forgot-password
post('/internal/forgot-password', {
  handler: handlers.sendForgotPasswordEmailController,
  schema: Joi.object<Auth.HttpRequest.SendForgotPasswordEmail>({
    email: Joi.string().email().required()
  })
});

// Reset Password => POST :: /auth/internal/reset-password
post('/internal/reset-password', {
  handler: handlers.resetPasswordController,
  schema: Joi.object<Auth.HttpRequest.ResetPassword>({
    resetPasswordToken: Joi.string().email().required(),
    password: Joi.string().email().required(),
    passwordConfirmation: Joi.string().valid(Joi.ref('password')).required()
  })
});

export const routes = router;