import sendgridMail from '@sendgrid/mail';
import { env } from '../config/env';

interface EmailParams {
  to: string,
  from: string,
  subject: string,
  html: string
}

sendgridMail.setApiKey(env.sendgridApiKey);

export const sendEmail = async ({ from, to, html, subject }: EmailParams): Promise<void> => {
  await sendgridMail.send({ to, from, subject, html });
}