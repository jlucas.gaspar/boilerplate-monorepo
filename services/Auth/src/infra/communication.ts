import { createUserService } from '@monorepo-boilerplate/communication/user';
import { env } from '../config/env';

const { internalApiKey, mode } = env;

export const userService = createUserService({ internalApiKey, mode });