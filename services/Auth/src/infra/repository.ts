import { initDatabase } from '@monorepo-boilerplate/database/knex';
import { Auth } from '@monorepo-boilerplate/types/auth';
import { env } from '../config/env';
import knexfile from '../../knexfile';

type Model = Auth.Model.ResetPassword;

const database = initDatabase(env, knexfile);
const resetPasswordTokenTable = () => database<Model>('resetPasswordTokens');

export const insertResetPasswordToken = async (email: string): Promise<Model> => {
  return await resetPasswordTokenTable().insert({ email }).returning('*').then(resetPasswordToken => resetPasswordToken[0]);
}

export const findResetPasswordTokenByToken = async (resetPasswordToken: string): Promise<Model | undefined> => {
  return await resetPasswordTokenTable().where('resetPasswordToken', resetPasswordToken).first();
}