import * as resetPasswordTokenRepository from '../infra/repository';
import { sendEmail } from '../infra/email';
import { env } from '../config/env';

export const sendForgotPasswordEmail = async (email: string): Promise<string> => {
  const { resetPasswordToken } = await resetPasswordTokenRepository.insertResetPasswordToken(email);

  await sendEmail({
    from: 'joselucas.gaspar@gmail.com',
    to: 'joselucas.gaspar@gmail.com',
    subject: 'Teste',
    html: `<a href='${env.frontendUrl}/nova-senha/${resetPasswordToken}'>Clique aqui</a>`
  });

  return `E-mail enviado para ${email}.`;
}