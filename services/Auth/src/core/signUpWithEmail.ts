import { BadRequestError } from '@monorepo-boilerplate/error';
import { generateJwt } from '@monorepo-boilerplate/jwt';
import { Auth } from '@monorepo-boilerplate/types/auth';
import { userService } from '../infra/communication';
import { encryptPassword } from '../infra/encrypter';
import { env } from '../config/env';

type Input = Auth.Core.SignUpWithEmail;
type Output = Auth.Helpers.JwtToken;

export const signUpWithEmail = async ({ email, password, name }: Input): Promise<Output> => {
  const emailAlreadyInUse = await userService.getUserByEmail({ email });
  if (emailAlreadyInUse) {
    throw new BadRequestError('E-mail já está em uso.');
  }

  const newPassword = await encryptPassword(password);
  const { id } = await userService.createUser({ email, password: newPassword, provider: 'google', name });

  return generateJwt(id, env.jwtSecret);
}