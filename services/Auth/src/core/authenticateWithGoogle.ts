import { Auth } from '@monorepo-boilerplate/types/auth';
import { BadRequestError } from '@monorepo-boilerplate/error';
import { userService } from '../infra/communication';
import { generateJwt } from '@monorepo-boilerplate/jwt';
import { env } from '../config/env';

type Input = Auth.Core.AuthenticateWithGoogle;
type Output = Auth.Helpers.JwtToken;

export const authenticateWithGoogle = async ({ email, providerId, name, photoUrl, phoneNumber }: Input): Promise<Output> => {
  const provider: 'google' = 'google';

  const user = await userService.getUserByEmail({ email });

  const nameOrEmail = name ? name : email;

  if (!user) {
    const { id } = await userService.createUser({ email, provider, providerId, photoUrl, phoneNumber, name: nameOrEmail });
    return generateJwt(id, env.jwtSecret);
  }

  if (user.provider !== 'google') {
    const updateData = { id: user.id, provider, providerId, password: null, name: nameOrEmail }
    if (phoneNumber) Object.assign(updateData, { phoneNumber });
    if (photoUrl) Object.assign(updateData, { photoUrl });
    await userService.updateUser(updateData);
  }

  if (user.provider === 'google' && user.providerId !== providerId) {
    throw new BadRequestError(`Erro ao logar o usuário de e-mail ${email}.`);
  }

  return generateJwt(user.id, env.jwtSecret);
}