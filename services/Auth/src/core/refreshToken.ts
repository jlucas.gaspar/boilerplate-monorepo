import { generateJwt } from '@monorepo-boilerplate/jwt';
import { Auth } from '@monorepo-boilerplate/types/auth';
import { env } from '../config/env';

type Input = Auth.HttpRequest.RefreshToken;
type Output = Auth.Helpers.JwtToken;

export const refreshToken = ({ userId }: Input): Output => {
  return generateJwt(userId, env.jwtSecret);
}