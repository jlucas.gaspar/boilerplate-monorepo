import { Auth } from '@monorepo-boilerplate/types/auth';
import { BadRequestError } from '@monorepo-boilerplate/error';
import { generateJwt } from '@monorepo-boilerplate/jwt';
import { ensureResetPasswordTokenIsValid } from '../utils/ensureResetPasswordTokenIsValid';
import { findResetPasswordTokenByToken } from '../infra/repository';
import { encryptPassword } from '../infra/encrypter';
import { userService } from '../infra/communication';
import { env } from '../config/env';

type Input = Auth.Core.ResetPassword;
type Output = Auth.Helpers.JwtToken;

export const resetPassword = async ({ password, resetPasswordToken }: Input): Promise<Output> => {
  const token = await findResetPasswordTokenByToken(resetPasswordToken);
  if (!token) {
    throw new BadRequestError('Token de recuperação de senha não existe.');
  }

  const isValid = ensureResetPasswordTokenIsValid(token.createdAt);
  if (!isValid) {
    throw new BadRequestError('Token expirado. Gere outro para seu e-mail.');
  }

  const user = await userService.getUserByEmail({ email: token.email });
  if (!user) {
    throw new BadRequestError('Erro ao encontrar o usuário da requisição desse token.');
  }

  const hashedPassword = await encryptPassword(password);

  const userData = {};

  if (user.provider !== 'email') {
    Object.assign(userData, { provider: 'email', providerId: null });
  }

  await userService.updateUser({
    ...userData,
    id: user.id,
    password: hashedPassword
  });

  return generateJwt(user.id, env.jwtSecret);
}