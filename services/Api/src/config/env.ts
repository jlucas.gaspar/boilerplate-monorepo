import { getEnv, initializeDotenv } from '@monorepo-boilerplate/env';

initializeDotenv();

const myEnv = process.env;

export const env = {
  mode: getEnv('MODE', myEnv),
  internalApiKey: getEnv('INTERNAL_API_KEY', myEnv),
  jwtSecret: getEnv('JWT_SECRET', myEnv)
}