import { Http } from '@monorepo-boilerplate/http/types';
import * as core from '../core';

type Input = Http.Request<void>;
type Output = Http.Response<any>;

export const pingController = async (request: Input, response: Output): Promise<Output> => {
  const pong = await core.ping(request.body);
  return response.status(200).json(pong);
}